# Split-method-in-Java



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.StringTokenizer;



public class Split{
 
   public static void main(String[] args){
    BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
    boolean done = false;
    
    while(!done)
    {
        String inputLine = console.readLine();
        if (inputLine == null)
        done = true;
       else
       {
           // break input line into words
           
           StringTokenizer tokenizer = new StringTokenizer(inputLine);
           while (tokenizer.hasMoreTokens())
           {
             // print each word
             String word = tokenizer.nextToken();
             System.out.println(word);
          }
         }    
       }     
     }  
   }  
